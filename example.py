import os
import time
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options



display = Display(visible=0, size=(800, 600))
display.start()

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox')

chrome_options.add_experimental_option('prefs', {
    'download.default_directory': os.getcwd(),
    'download.prompt_for_download': False,
})

browser = webdriver.Chrome(chrome_options=chrome_options)
browser.get('https://accounts.google.com/signin/v2/identifier?service=accountsettings&hl=en-US&continue=https%3A%2F%2Fmyaccount.google.com%2Fintro&csig=AF-SEnZ8MXs4iCcjkqXk%3A1591188549&flowName=GlifWebSignIn&flowEntry=ServiceLogin')
time.sleep(5)
WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'input[type="email"]'))).send_keys('***@gmail.com' + Keys.ENTER)
time.sleep(10)
WebDriverWait(browser, 60).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'input[type="password"]'))).send_keys('***' + Keys.ENTER)
time.sleep(10)

print(browser.title)
print(browser.page_source)

browser.quit()
display.stop()